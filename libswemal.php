<?php

// libswemal version 0.1.1
// Copyright 2017  Ulf E. Lundin
// Released as open source under the terms of the BSD 2-clause "Simplified" License


function logg($lh, $txt)
// logs a line of text to the log file
{
  $txt2 = str_replace("\r", "\\r", $txt);
  $txt2 = str_replace("\n", "\\n", $txt2);
  $line = date(DATE_ATOM) . ' ' . $txt2 . "\n";
  fputs($lh, $line);
}


function curly($url, $referer, $useragent)
// fetches a url using command-line curl with lots of options
{
  $url = preg_replace('%^-+%', '', $url); // remove command-line options
  $referer = preg_replace('%^-+%', '', $referer);

  $cmd = "curl --compressed --insecure --location --silent --connect-timeout 7 " .
         "--max-time 14 --max-redirs 5 --max-filesize 2017000 " .
         "--header 'Accept: image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*' " .
         "--header 'Accept-Language: en-US' " .
         "--header 'Connection: Keep-Alive' " .
         "--header ". escapeshellarg('Referer: ' . $referer) . ' ' .
         "--proto-redir '-all,http,https' " .
         "--user-agent ". escapeshellarg($useragent) . ' ' .
         escapeshellarg($url);

  $out = array();
  exec($cmd, $out);
  $outstr = join("\n", $out);

  return $outstr;
}


function eitest($data)
// looks for different versions of the eitest redirection in fetched data
{
  $regex =
    '%<script\s+type="text/javascript">\s*' .
    '(var\s+[a-z]{3,8}\s*=\s*"iframe";\s*)?' .
    '(var\s+[a-z]{3,8}\s*=\s*"(https?://[^"]*|(\%u[0-9a-f]{4})+)";\s*)?' .
    'var\s+[a-z]{3,8}\s*=\s*document\.createElement\(("iframe"|[a-z]{3,8})\);\s*' .
    '(var\s+[a-z]{3,8}\s*=\s*"";\s*)?' .
    '[a-z]{3,8}\.style\.width\s*=\s*"[0-9]{1,2}px";\s*[a-z]{3,8}\.style\.' .
    'height\s*=\s*"[0-9]{1,2}px";\s*[a-z]{3,8}\.style\.border\s*=\s*"[0-9]px";' .
    '\s*[a-z]{3,8}\.frameBorder\s*=\s*"[0-9]";\s*[a-z]{3,8}\.setAttribute\(' .
    '"frameBorder",\s*"[0-9]"\);\s*document\.body\.appendChild\([a-z]{3,8}\);\s*' .
    '([a-z]{3,8}\s*=\s*"https?://[^"]*";\s*)?' .
    '[a-z]{3,8}\.src\s*=\s*(unescape\()?[a-z]{3,8}\)?\;\s*</script>%i';

  if (preg_match($regex, $data))
    return 'eitest';
  else
    return '';
}


function pseudodarkleech($data)
// looks for the pseudodarkleech redirection in fetched data
{
  $regex = '%<span\s+style="position:absolute;\s+top:-[0-9]{3,4}px;\s+width:' .
           '[0-9]{3,4}px;\s+height:[0-9]{3,4}px;">\s*' .
           '[a-z]{2,10}\s*<iframe\s+src="https?://[^"]*"\s+width="[0-9]{3,4}"' .
           '\s+height="[0-9]{3,4}">\s*</iframe>\s*' .
           '[a-z]{2,10}\s*</span>\s*[a-z]{2,10}\s*<noscript>%i';

  if (preg_match($regex, $data))
    return 'pseudodarkleech';
  else
    return '';
}


function rig($data)
// looks for rig exploit kit urls in fetched data
// note: their patterns change quite often
{
  return ''; // FIXME write code for new RIG URLs
}


function statdot($data)
// looks for the joomla statdot attack in fetched data
{
  $regex = '%<script\s+language=javascript\s+src=(/media/system/js/stat[0-9a-z]{3}\.php)\s*></script>%i';

  if (preg_match($regex, $data, $matches))
    return "joomla statdot $matches[1]";
  else
    return '';
}


function defaced($data)
// looks for a few variants of website defacement in fetched data
{
  $regex = '%(by\s+w4l3XzY3|Cyb3r-Shia|HackeD By TeaM_CC :: 0x0 WAS HERE|Hacked By Not Matter who am i . i am white Hat Hacker please update your wordpress|Hmei7|d3b~X|' .
           'Hacked By (El Behram|Alarg53|Rj_d3))%';

  if (preg_match($regex, $data))
    return 'defaced';
  else
    return '';
}


function eitestsoceng($data)
// looks for eitest soceng (hoeflertext) in fetched data
{
  $regex = '%<img\s+id="l0gos"\s+alt=..\s*/><p\s+id="pphh"\s*>The "HoeflerText" font wasn.t found\.</p></div>%';

  if (preg_match($regex, $data))
    return 'eitest soceng';
  else
    return '';
}


function dodgy($data)
// looks for various dodgy things in fetched data
// both used for testing new stuff and for known, less important attacks
{
  $regex = '%(trafficanalytics\.online|left:-1275px; top:0|code\.wordprssapi\.com|<center>\s*<iframe[^>]*src="https?:[^>]* seamless="seamless">|' .
           '<iframe src=\x27.*?\x27width=\x27250\x27 height=\x27250\x27>)%i';

  if (preg_match($regex, $data, $matches))
    return 'dodgy ' . $matches[1];
  else
    return '';
}


function toplevel($data)
// generic: looks for urls with suspicious top-level domains in fetched data
{
   preg_match_all('%(https?:\\\\?/\\\\?/[-.a-z0-9]+\.(top|xyz|pro|info|adult|sex|xxx|club|download|zip|review|country|kim|link|pw|ml))[^-.a-z0-9]%i', $data, $matches);
   if (count($matches[1]) !== 0)
     return 'toplevel ' . join(' , ', $matches[1]);
   else
     return '';
}


function ipurl($data)
// generic: looks for urls with ipv4 addresses in fetched data
{
   preg_match_all('%(https?:\\\\?/\\\\?/[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)[^0-9]%i', $data, $matches);
   if (count($matches[1]) !== 0)
     return 'ipurl ' . join(' , ', $matches[1]);
   else
     return '';
}


function testdata($data, $lh, $urlline)
// looks for all the above problems in fetched data
// if anything is found, the data is saved and a note is logged
{
  $resul = array();
  $resul_et = eitest($data);
  if ($resul_et !== '')
    array_push($resul, $resul_et);

  $resul_pd = pseudodarkleech($data);
  if ($resul_pd !== '')
    array_push($resul, $resul_pd);

  $resul_rv = rig($data);
  if ($resul_rv !== '')
    array_push($resul, $resul_rv);

  $resul_sd = statdot($data);
  if ($resul_sd !== '')
    array_push($resul, $resul_sd);

  $resul_df = defaced($data);
  if ($resul_df !== '')
    array_push($resul, $resul_df);

  $resul_es = eitestsoceng($data);
  if ($resul_es !== '')
    array_push($resul, $resul_es);

  $resul_dg = dodgy($data);
  if ($resul_dg !== '')
    array_push($resul, $resul_dg);

  $resul_tl = toplevel($data);
  if ($resul_tl !== '')
    array_push($resul, $resul_tl);

  $resul_iu = ipurl($data);
  if ($resul_iu !== '')
    array_push($resul, $resul_iu);

  $resulstr = join(' :: ', $resul);
  if ($resulstr !== '') {
    logg($lh, " *FOUND STUFF*  $urlline  $resulstr");
    $urlline2 = preg_replace('%[/:?&=]%', '_', $urlline);
    $urlline2 = substr($urlline2, 0, 128);

    $sh = fopen('save/' . $urlline2, 'w') or die("can't open");
    fputs($sh, "$resulstr\n$data\n");
    fclose($sh) or die("can't close");
  }
}


function normalize($url)
// normalizes urls with path traversals
// i.e. "http://a/b/c/../d" becomes "http://a/b/d"
{
  $urlarr = explode('/', $url);
  $urlarrnew = array();

  foreach ($urlarr as $oneurlpart) {
    if ($oneurlpart === '..') {
      if (count($urlarrnew) > 0)
        array_pop($urlarrnew);
    } else {
      array_push($urlarrnew, $oneurlpart);
    }
  }

  return join('/', $urlarrnew);
}


function followtags($url, $data, $lh, $urlcache, $useragent)
// follows script/iframe tags in fetched data
{
  // parse the url into its components, so we can handle relative links like "/no/host/name" or "file.txt"

  $urldata = parse_url($url);
  $urlstart = $urldata['scheme'] . '://' . $urldata['host'];
  if (isset($urldata['port']))
    $urlstart .= ':' . $urldata['port'];
  $urlpath = $urldata['path'];
  $urlpath = preg_replace('%/[^/]*$%', '/', $urlpath);

  // parse the html for script and iframe tags

  preg_match_all('%<(script|iframe)([^>]+)>%i', $data, $matches);
  $sources = array();
  foreach ($matches[2] as $onematch) {
    $splits = preg_split('%\s+%', $onematch);
    foreach ($splits as $onesplit) {
      if (preg_match('%^src=["\']([^"\']+)["\']%i', $onesplit, $matches2)) { // src attribute
        if (substr($matches2[1], 0, 2) === '//') // same scheme
          array_push($sources, $urldata['scheme'] . ':' . $matches2[1]);
        elseif (preg_match('%^https?://%', $matches2[1])) // full url
          array_push($sources, $matches2[1]);
        elseif (substr($matches2[1], 0, 1) === '/') // starts with "/"
          array_push($sources, $urlstart . $matches2[1]);
        else // just a filename
          array_push($sources, $urlstart . $urlpath . $matches2[1]);
      }
    }
  }

  // fetching and testing the sources

  $sourcecount = 0;
  foreach ($sources as $onesource) {
    $onesource3 = preg_replace('%&amp;%', '&', $onesource); // fix up some html entities
    $onesource3 = preg_replace('%&#0*38;%', '&', $onesource3);

    $onesourcekeep = $onesource3;
    $onesource3 = normalize($onesource3);
    if ($onesource3 !== $onesourcekeep)
      logg($lh, ' url "' . $onesourcekeep . '" got normalized to "' . $onesource3 . '"');

    logg($lh, ' follow tags for ' . $url . ', fetching ' . $onesource3);
    $onesource2 = preg_replace('%\?.*$%', '', $onesource3); // remove the query parameters from the url before caching it
    $onesource2 = strtoupper($onesource2);

    if (isset($urlcache[$onesource2]))
      logg($lh, ' already found in the cache - skipping');
    else {
      $urlcache[$onesource2] = 'x';

      $srcdata = curly($onesource3, $url, $useragent); // fetch the new url
      $srcdata = substr($srcdata, 0, 512000);
      testdata($srcdata, $lh, $onesource3); // test its data
      $sourcecount++;
    }

    if ($sourcecount > 12) {
      logg($lh, ' too many tags to follow, skipping');
      break;
    }
  }

  return $urlcache;
}

